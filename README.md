# Building Blacklist
## (Modified Building Rights)
**_An add-on for the indie game Blockland._**


Prevent players from building without permission in whitelist mode, or ban specific
players from building in blacklist mode.
