// Server_BuildBlacklist
// ~/BLGPrefs.cs
//======================================================================================


//======================================================================================
//	Basic Blockland Glass Preferences

// Register the Addon
registerPreferenceAddon("Server_BuildBlacklist", "Building Rights", "brick_edit");

// Mode Preferece. Control whether or not Whitelist is enabled
new ScriptObject(Preference)
{
	className      = "WhitelistModePref"; //namespace

	addon          = "Server_BuildBlacklist"; //add-on filename
	category       = "Building Rights";
	title          = "Mode";

	type           = "dropdown";
	params         = "Blacklist 0 Whitelist 1";

	variable       = "$Pref::Server::BuildingBlacklist::WhitelistMode"; //global variable (optional)

	defaultValue   = "0";

	updateCallback = "WhitelistModePrefUpdate"; //to call after ::onUpdate (optional)
	loadCallback   = ""; //to call after ::onLoad (optional)

	hostOnly       = false; //default false (optional)
	secret         = true; //whether to tell clients the value was updated (optional)

	loadNow        = false; // load value on creation instead of with pool (optional)
	noSave         = false; // do not save (optional)
	requireRestart = false; // denotes a restart is required (optional)
};

// Admin can Still build pref: Whether or not admins can still build with certain levels
new ScriptObject(Preference)
{
	className      = "BuildingAdminsPref"; //namespace

	addon          = "Server_BuildBlacklist"; //add-on filename
	category       = "Admin Rights";
	title          = "Admins Can Build Without Rights";

	type           = "bool";
	params         = "0 1";

	variable       = "$Pref::Server::BuildingBlacklist::BannedAdminsBuild"; //global variable (optional)

	defaultValue   = "1";

	updateCallback = ""; //to call after ::onUpdate (optional)
	loadCallback   = ""; //to call after ::onLoad (optional)

	hostOnly       = true; //default false (optional)
	secret         = false; //whether to tell clients the value was updated (optional)

	loadNow        = false; // load value on creation instead of with pool (optional)
	noSave         = false; // do not save (optional)
	requireRestart = false; // denotes a restart is required (optional)
};


// Admin Level Pref: The admin level required to still be able to build without rights
new ScriptObject(Preference)
{
	className      = "AdminLevelPref"; //namespace

	addon          = "Server_BuildBlacklist"; //add-on filename
	category       = "Admin Rights";
	title          = "Admin Rights Level (requires above to be on)";

	type           = "dropdown";
	params         = "Admin 1 Super-Admin 2 Host 3"; //list based parameters

	variable       = "$Pref::Server::BuildingBlacklist::AdminRequirement"; //global variable (optional)

	defaultValue   = "Admin 1";

	updateCallback = ""; //to call after ::onUpdate (optional)
	loadCallback   = ""; //to call after ::onLoad (optional)

	hostOnly       = true; //default false (optional)
	secret         = false; //whether to tell clients the value was updated (optional)

	loadNow        = false; // load value on creation instead of with pool (optional)
	noSave         = false; // do not save (optional)
	requireRestart = false; // denotes a restart is required (optional)
};

// Clearing the list of whitelist permissions
new ScriptObject(Preference)
{
	className      = "ClearWhiteListPref"; //namespace

	addon          = "Server_BuildBlacklist"; //add-on filename
	category       = "Reset";
	title          = "Clear Whitelist Permissions";

	type           = "bool";
	params         = "0 1";

	variable       = "$WhitelistClear"; //global variable (optional)

	defaultValue   = "0";

	updateCallback = "ClearWhiteListPrefUpdate"; //to call after ::onUpdate (optional)
	loadCallback   = ""; //to call after ::onLoad (optional)

	hostOnly       = true; //default false (optional)
	secret         = true; //whether to tell clients the value was updated (optional)

	loadNow        = false; // load value on creation instead of with pool (optional)
	noSave         = true; // do not save (optional)
	requireRestart = false; // denotes a restart is required (optional)
};

// clearing the list of blacklist bans
new ScriptObject(Preference)
{
	className      = "ClearBlacklistPref"; //namespace

	addon          = "Server_BuildBlacklist"; //add-on filename
	category       = "Reset";
	title          = "Clear Blacklist Bans";

	type           = "bool";
	params         = "0 1";

	variable       = "$BlacklistClear"; //global variable (optional)

	defaultValue   = "0";

	updateCallback = "ClearBlacklistPrefUpdate"; //to call after ::onUpdate (optional)
	loadCallback   = ""; //to call after ::onLoad (optional)

	hostOnly       = true; //default false (optional)
	secret         = true; //whether to tell clients the value was updated (optional)

	loadNow        = false; // load value on creation instead of with pool (optional)
	noSave         = true; // do not save (optional)
	requireRestart = false; // denotes a restart is required (optional)
};

// Set a global init value to true so we don't clutter the BLG Preference GUI
 $BuildBlacklistGlassInit = true;


 //======================================================================================
 //	Preference Update Handler

 function WhitelistModePrefUpdate(%this, %value)
 {
 	$Pref::Server::BuildingBlacklist::WhitelistMode = %value;
 	if ( $Pref::Server::BuildingBlacklist::WhitelistMode )
 	{
 		deactivatePackage(BuildingBlackList);
 		activatePackage(BuildingWhiteList);
 		chatMessageAll('',"\c6Building Whitelist is now enabled, you must now obtain building rights to build.");
 	}
 	else
 	{
 		activatePackage(BuildingBlackList);
 		deactivatePackage(BuildingWhiteList);
 		chatMessageAll('',"\c6Building Blacklist is now enabled, everyone can build unless you're banned.");
 	}
 }

function ClearWhiteListPrefUpdate(%this, %value)
{
    if ( %value )
    {
        $Pref::Server::BuildWhitelist = "";
        chatMessageAll('', "\c6All building rights have been cleared.");
    }
    $WhitelistClear = 0;
}

function ClearBlacklistPrefUpdate(%this, %value)
{
    if ( %value )
    {
        $Pref::Server::BuildBlacklist = "";
        chatMessageAll('', "\c6All building bans have been cleared.");
    }
    $BlacklistClear = 0;
}
